const express = require('express');
const app = express();
const hbs = require('hbs');
const fs = require('fs');

/** Vistas parciales donde se renderizarán los templates hijos, para hacer algo de DRY 
 * Dirname ser refiere al directorio raíz de la aplicación*/
hbs.registerPartials(__dirname + '/views/partials')
/** Kvp set, para utilizar handlebars como un template engine */
app.set('view engine', 'hbs');
/** Para usar middlewares.
 * 
 */

/** Middleware con use, (funciones) y se ejecutan en el orden en que se declaran o se 
 * adhieren 
 */
app.use((req, res, next) => {
  const log = new Date().toISOString() + ' ' + req.method + ' ' + req.url
  console.log(log);
  fs.appendFile('server.log', log + '\n', err => {
    if (err) console.log('Unable to append to server.log');
  })
  next();
})

app.use((req, res, next) => {
  Math.random() > 1 ?
    res.render('maintenance.hbs') :
    next();
});

app.use(express.static(__dirname + '/public'));
/** Helpers que son funciones que corren dentro del templating para no repetir mismos 
 * parámetros en la función res.reder. Funcionan como las computed properties (sortof)
 */
hbs.registerHelper('getYear', () => (new Date().getFullYear()));
hbs.registerHelper('screamIt', text => text.toUpperCase());

app.get('/', (req, res) => {
  res.render('index.hbs', {
    name: 'Joe',
    lastName: 'Hoe',
    pageTitle: 'Welcome',
    likes: ['Develop', 'Table Tennis'],
    message: `Welcome to my 90's website, cause i think template rendering should be dead! :D`,
  });
});

app.get('/about', (req, res) => {
  /** Res.reder renderiza mediante el engine el html, en el formato de handlebars para 
   * enviarlo como html puro al cliente, 
   * El primer parámetro de render es la referencia a la vista (nota: siempre estarán
   * dentro de la carpeta views), el segundo parámetro son los datos que dinámicamente se
   * inyectarán al html
   */
  res.render('about.hbs', {
    pageTitle: 'About Page',
  });
});

app.get('/bad', (req, res) => {
  res.status(404).json({
    errorMessage: 'Page not found'
  });
})

app.listen(3000, () => console.log('Server is up on port 3000'));
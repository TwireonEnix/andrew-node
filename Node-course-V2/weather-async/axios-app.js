require('dotenv').config();
const yargs = require('yargs');
const axios = require('axios');
const geocode = require('./geocode/geocode');
const argv = yargs
  .option({
    address: {
      demand: true,
      alias: 'a',
      describe: 'Address to fetch the weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h').argv;

const encodedAddress = encodeURIComponent(argv.address),
  geocodeURL = `http://www.mapquestapi.com/geocoding/v1/address?key=${process.env.WEATHER_KEY}&location=${encodedAddress}`;

axios.get(geocodeURL).then(response => {
    /** Por la manera en la que ahora trabaja el api de mapquest, no es posible aplicar
     * lo mismo que en el curso, ya que la respuestas del server no son las mismas
     */
    if (response.data.status === 'ZERO RESULTS') {
      /** Esto hará que el resto de este bloque de código no sea ejecutado y se pasa
       * directamente al catch
       */
      throw new Error('Unable to find that address.');
    }
    const simplified = response.data.results[0].locations[0],
      address = `${simplified.street},
            ${simplified.adminArea5},
            ${simplified.adminArea4},
            ${simplified.adminArea3},
            ${simplified.adminArea2},
            ${simplified.adminArea1},
            ${simplified.postalCode}`,
      weatherURL = `https://api.darksky.net/forecast/${process.env.FORECAST_API_KEY}/${simplified.latLng.lat},${simplified.latLng.lng}`;
    // console.log(JSON.stringify(response.data.results[0], null, 2))
    console.log(`In ${address}`);
    return axios.get(weatherURL);
  }).then(response => {
    const weather = response.data.currently;
    console.log(`It's ${weather.summary} there and the temperature is ${geocode.changeRetardUnits(weather.temperature)}°C / ${weather.temperature}°F.
      There is ${weather.humidity * 100}% chance of raining!`);
  })
  .catch(e => console.log(e.message));
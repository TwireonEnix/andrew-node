require('dotenv').config();
// const request = require('request');
const yargs = require('yargs');
const geocode = require('./geocode/geocode');

/** el body es lo que siempre regresa de una petición http.
 * statusCode: cómo fue el request, (200) de ok
 * headers: parte del protocolo http kvps, seteados por el api, metadata de la comunicación
 *
 *  location=19.6338097,-99.2194893`,
 */

/** Opciones de yargs, objeto de js, demand requiere el dato, describe lo que aparece cuando se require
 * help (agregado encadenando la función), string hace que el dato que reciba sea necesariamente un string,
 * despues de ese pipe de funciones se guarda el arreglo de argv en la constante.
 */
const argv = yargs
  .option({
    address: {
      demand: true,
      alias: 'a',
      describe: 'Address to fetch the weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h').argv;

// console.log(argv);

geocode.geoAddress(argv.address, (errorMessage, results) => {
  if (errorMessage) {
    console.log(errorMessage);
    return;
  }
  console.log(JSON.stringify(results.address, null, 2));
  geocode.geoWeather(results, (error, weather) => {
    if (error) {
      console.log(error);
      return;
    }
    console.log(
      `It's ${
        weather.summary
      } there and the temperature is ${geocode.changeRetardUnits(
        weather.temperature
      )}°C / ${weather.temperature}°F.
      There is ${weather.humidity * 100}% chance of raining!`
    );
  });
});
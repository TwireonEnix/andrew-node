const request = require('request');
module.exports.geoAddress = (address, cb) => {
  request({
      url: `http://www.mapquestapi.com/geocoding/v1/address?key=${process.env.WEATHER_KEY}&location=${encodeURIComponent(address)}`,
      // }&location=1301%20lombard%20street%20philadelphia`,
      json: true
    },
    /** body viene con todo el metadata del response, pero request los separa en otro */
    (error, response, body) => {
      /** con los parámetros del stringify se puede ver el objeto completo, con el tercer
       * argumento.
       */
      // console.log(JSON.stringify(body, undefined, 2));
      let simplified;
      if (error) {
        cb(error, null);
      } else {
        simplified = body.results[0].locations[0];
      }
      if (simplified) {
        cb(null, {
          address: `${simplified.street}, ${simplified.adminArea5}, ${
            simplified.adminArea4
          }, ${simplified.adminArea3}, ${simplified.adminArea2}, ${
            simplified.adminArea1
          }, ${simplified.postalCode}`,
          latitude: simplified.latLng.lat,
          longitude: simplified.latLng.lng
        });
        // console.log(`Lat-Long: ${JSON.stringify(simplified.latLng, null, 2)}`);
      } else {
        cb(`Unable to find that address`);
      }
    }
  );
};

module.exports.geoWeather = (latLng, cb) => {
  request({
      url: `https://api.darksky.net/forecast/${process.env.FORECAST_API_KEY}/${
        latLng.latitude
      },${latLng.longitude}`,
      json: true
    },
    (error, response, body) => {
      if (error) cb(error, null);
      cb(null, body.currently);
    }
  );
};

module.exports.changeRetardUnits = temp => Math.trunc(((temp - 32) * 5) / 9);
/** Convert geoCode cb of request object into a functioning promise to call */
require('dotenv').config({
  path: '../.env'
});

const request = require('request');

const geocodeAddress = (address) => {
  return new Promise((resolve, reject) => {
    request({
      url: `http://www.mapquestapi.com/geocoding/v1/address?key=${process.env.WEATHER_KEY}&location=${encodeURIComponent(address)}`,
      json: true
    }, (e, response, body) => {
      let simplified;
      e ? reject(e) : simplified = body.results[0].locations[0];
      simplified ?
        resolve({
          address: `${simplified.street},
            ${simplified.adminArea5},
            ${simplified.adminArea4},
            ${simplified.adminArea3},
            ${simplified.adminArea2},
            ${simplified.adminArea1},
            ${simplified.postalCode}`,
          latitude: simplified.latLng.lat,
          longitude: simplified.latLng.lng
        }) : reject('Unable to find Address');
    })
  });
}

geocodeAddress('00000').then(location => console.log(JSON.stringify(location, undefined, 2))).catch(e => console.log(e));
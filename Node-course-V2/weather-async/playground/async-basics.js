console.log(`Starting app`);

setTimeout(() => {
  console.log(`Job done`);
}, 2000);

/** Aunque tenga un delay de 0 ms, la siguiente línea se ejecuta primero por la manera
 * en la que Node funciona.
 */
setTimeout(() => {
  console.log(`Hey`);
}, 0);

console.log(`Finishing up`);

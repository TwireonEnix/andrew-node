/** Introducción a promises */
/** Los estados de una promesa son resolve, cuando la promesas se completó con éxito
 * y el reject es para arrojar un error que ocurrió durante la ejecución del programa.
 * Resolve y reject sólo regresan un objeto, o dato. Entonces para enviar varios hay que
 * regresar un objeto js con distintas propiedades kvp
 */

/** funcionalidad simulando llamadas al servidor para sumar dos números
 */
const asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      typeof a === 'number' && typeof b === 'number' ? resolve(a + b) : reject('Inputs are not numbers!');
    }, 1500);
  });
};

const somePromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Hey. This succeded');
    reject('Unable to fulfill promise');
  }, 2500);
  // Math.random() > 0.5 ? resolve('Hey. This succeded') : reject(`Hey. This failed`);
});

/** Al llamar el método then del objeto promesa, ejecuta el código asíncrono. Para atrapar
 * cualquier vertiente de la ejecución, ya sean los datos requeridos o el error, se debe
 * pasar como argumento una función de callback, la original para el resolve, y después de 
 * un catch para el reject. Andrew no utilizó al inicio un catch, sino que pasó como segundo
 * argumento la función que se ejecuta en lugar del error:
 * promise.then(() => resolved, () => rejected);
 * Sin embargo, no se respeta el DRY cuando hay errores si es que hay encadenamiento 
 * de promesas.
 * Las promesas o resuelven o rechazan, no es posible hacer las dos cosas, o hacer dos
 * callbacks al mismo tiempo. Una vez que termine el cómputo de la promesa, ésta pasa
 * a un estado de settled donde 
 * 
 */
// somePromise.then((message) => console.log('Success ' + message)).catch((e) => console.warn(e));

/** Primera llamada */
asyncAdd(15, 1).then((val) => console.log(`Sum is ${val}`)).catch((e) => console.log(e));

asyncAdd(3, 5)
  .then(val => {
    console.log(`This should be 8: ${val}`);
    return asyncAdd(val, 5);
  })
  .then(val2 => console.log(`Final sum is ${val2}`))
  .catch(e => console.log(e));
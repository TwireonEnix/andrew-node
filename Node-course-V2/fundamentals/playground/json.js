// const obj = {
//   name: 'Andrew'
// };

/** stringigy parsea cualquier objeto js a un string, haciendolo capaz de  */
// const stringObj = JSON.stringify(obj);
// console.log(typeof stringObj);
// console.log(stringObj);
// console.log(JSON.parse(stringObj));

const fs = require('fs');
// console.log(fs);

const originalNote = {
  title: 'Some awesome title',
  body: 'JS is awesome'
};

const originalNoteString = JSON.stringify(originalNote);
/** original Note String */

fs.writeFileSync('notes.json', originalNoteString);
const noteString = fs.readFileSync('notes.json');
/** note */
const note = JSON.parse(noteString);

console.log(typeof note);
console.log(note.title);

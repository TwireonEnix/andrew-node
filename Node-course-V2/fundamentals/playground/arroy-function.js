/** El return está implícito si el statement es de una línea
 * equivalente x => { return x * *; }
 */
const square = x => x * x;

console.log(square(5));

/** arrow functions no hacen bind a la palabra reservada this. por lo tanto no funcionan dentro 
 * del statement out of the box, el this haría referencia a la función implícita de todo el 
 * documento. 
 */
const user = {
  name: 'Andrew',
  sayHi: () => console.log(`Hi ${this.name}`), //undefined
  /** Aquí sí funciona el bind de this. */
  sayHiAlt() {
    console.log(`Hi I'm ${this.name}`); // Hi I'm Andrew
  }
}

user.sayHi();
user.sayHiAlt();

console.log(arguments);
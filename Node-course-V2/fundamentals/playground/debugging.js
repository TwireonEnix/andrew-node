const person = {
  name: 'Dar'
};

// se usa como un breakpoint para utilizar el inspect
debugger;

person.age = 25;
person.name = 'Nav';

console.log(person);
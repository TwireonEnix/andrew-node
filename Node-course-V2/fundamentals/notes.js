// console.log('file was executed notes.js');
// module.exports.addNote = () => {
//   console.log('addNote');
//   return 'New Note';
// };

const fs = require('fs');

const fetchNotes = () => {
  try {
    const noteString = fs.readFileSync('notes-data.json');
    return JSON.parse(noteString);
  } catch (e) {
    return [];
  }
};

const saveNotes = (notes) => {
  fs.writeFileSync('notes-data.json', JSON.stringify(notes));
}

module.exports = {
  add: (a, b) => a + b,

  addNote: (title, body) => {
    const notes = fetchNotes();
    const note = {
      title,
      body
    };

    const duplicateNotes = notes.filter(note => note.title === title);
    if (duplicateNotes.length === 0) {
      notes.push(note);
      saveNotes(notes);
      // My easy solution, Mead creates an if in the app.js 
      console.log(`Note successfully created
      title: ${note.title}, body: ${note.body}`);
    } else {
      console.log('Note title already taken');
    }
  },

  getAll: () => {
    const notes = fetchNotes();
    console.log(`Printing ${notes.length} notes.`);
    for (const note of notes) {
      console.log(`title: ${note.title}, body: ${note.body}`);
    }
  },

  getNote: title => {
    const note = fetchNotes().filter(note => note.title === title)[0];
    note ? console.log(note) : console.log('Note not found');
  },

  removeNote: title => {
    const notes = fetchNotes();
    const filteredNotes = notes.filter(note => note.title !== title);
    saveNotes(filteredNotes);
    notes.length !== filteredNotes.length ?
      console.log('Removed note', title) :
      console.log('No note with that title');
  }
};
// console.log('Starting app.js');

/** fs stands for file system */
const fs = require('fs');
const os = require('os');

/** Parsing de los argumentos de entrada de un proceso de node */
const yargs = require('yargs');
// const user = os.userInfo();
const notes = require('./notes');

/** Bunch of utitities */
const _ = require('lodash');
// const bcrypt = require('bcryptjs');
/** crea o sigue escribiendo sobre el mismo archivo si ya existe un string provisto
 * String templating para hacer concatenación de manera más natural.
 */

// bcrypt
//   .genSalt(10)
//   .then(salt => bcrypt.hash('Kodistos2018', salt))
//   .then(hash => console.log(hash));

// console.log(_.isString(true));
// console.log(_.isString('true'));

/** Configuración de yargs adicional, hay cosas que configurasr como objetos como argumento del command,
 * sin embargo, no me interesa aprender yargs, por eso lo skipe
 */
// const argv = yargs.command().help().argv;
const argv = yargs.argv;

/** yargs permite trabajar mejor con argumentos ingresados en la consola, los parsea como un objeto
 * y la propiedad que los maneja es una propiedad dentro de ese objeto tipo arreglo llamada _
 *  { _: [], '$0': 'app.js' }
 */
const command = argv._[0];
// console.log(argv);{ _: [], '$0': 'app.js' }


switch (command) {
  case 'add':
    const note = notes.addNote(argv.title, argv.body);
    break;
  case 'list':
    notes.getAll();
    break;
  case 'read':
    notes.getNote(argv.title);
    break;
  case 'remove':
    notes.removeNote(argv.title);
    break;
  default:
    console.log('Command not recognized');
    break;
}

// const filteredArray = _.uniq(['Dario', 'Dario', 2, 1, 3, 4]);
// console.log(filteredArray);

// fs.appendFile(
//   'greetings.txt',
//   `Hello ${user.username}
//   ${notes.addNote()}
//   suma: ${notes.add(15, 16)}
//   `,
//   err => {
//     if (err) {
//       console.log(err);
//     }
//   }
// );
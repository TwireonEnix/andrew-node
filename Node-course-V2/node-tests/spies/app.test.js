const expect = require('expect');
/** rewire es una utilizada de testing para cargar otros módulos sobre él, 
 * rewire carga módulos con require internamente, pero les hace un mockup y 
 * carga dos métodos extras sobre ellos __get__ __set__ la cual sirve para 
 * crear simulaciones
 */
const rewire = require('rewire');

const app = rewire('./app');


/** Los spies son funciones que pueden análizar las llamadas que se hacen a otras
 * funciones y hacer aserciones basadas en los argumentos y en el contexto en los que
 * son llamadas.
 */
describe('Testing spies', () => {

  const db = {
    saveUser: expect.createSpy()
  }
  app.__set__('db', db);

  it('should call the spy correctly', () => {
    const spy = expect.createSpy();
    spy('Andrew', 25);
    expect(spy).toHaveBeenCalled().toHaveBeenCalledWith('Andrew', 25);
  });

  /** Para verificar que otra función llama a otra */
  it('should apply a spy in app.js to ensure db.saveUser is called with the correct args', () => {
    const email = 'nav@example.com',
      password = 'abc123'
    app.handleSignIn(email, password);
    expect(db.saveUser).toHaveBeenCalledWith({
      email,
      password
    });
  });
});
const request = require('supertest');
const expectLib = require('expect');

const app = require('./app');

describe('Server tests', () => {

  describe('Root tests', () => {
    it('should return hello world response in json format', done => {
      /** Uso de supertest, ver docs. expect es muy flexible, puede recibir una función
       * como response y ver todas las propiedades del request o el response para posteriormente
       * pasarlo por el mismo pipe de validaciones de expect
       */
      request(app).get('/').expect(404).expect({
        msg: 'Hello Worlds'
      }).expect(res => {
        expectLib(res.body).toBeAn('object').toInclude({
          msg: 'Hello Worlds'
        });
      }).end(done);
    });
  });

  describe('User tests', () => {
    it('should return an array of users with name and age props and I should exist', done => {
      request(app).get('/users').expect(200).expect(res =>
        /** To include funciona en arrays y objetos */
        expectLib(res.body).toBeAn('array').toInclude({
          name: 'Darío',
          age: 28
        })
      ).end(done);
    });
  });

})
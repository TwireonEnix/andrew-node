const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.status(404).json({
    msg: 'Hello Worlds'
  });
});

app.get('/users', (req, res) => {
  res.status(200).json([{
      name: 'Darío',
      age: 28
    },
    {
      name: 'Alejandro',
      age: 25
    },
  ])
})

// app.listen(3000, () => console.log('Server on 3000'));
app.listen(3000);
module.exports = app;
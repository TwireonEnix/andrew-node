module.exports = {
  add: (a, b) => a + b,
  square: a => a ** 2,
  setName: (user, fullName) => {
    const names = fullName.split(' ');
    user.firstName = names[0];
    user.lastName = names[1];
    return user;
  },
  asyncAdd: (a, b, callback) => {
    setTimeout(() => {
      callback(a + b);
    }, 1000);
  },
  asyncSubtract: (a, b) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(a - b);
      }, 1500);
    })
  },
  // asyncSquare: a => {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       resolve(a ** 2);
  //     }, 500);
  //   })
  // }
  // Square more elegant version to avoid return keyword with implicit return
  asyncSquare: a => new Promise(resolve => {
    setTimeout(() => {
      resolve(a ** 2);
    }, 500)
  })
}
/** Behaviour driven development */
const expect = require('expect');
const utils = require('./utils');
/** Assert used by stephen */
// const assert = require('assert');

/** Los describes se pueden anidar */
describe('Utils set of tests', () => {

  describe('Sync', () => {
    it('should add two numbers', done => {
      const res = utils.add(15, 15);
      // if (res !== 30) throw new Error(`Expected 30 but got ${res}`);
      // assert(30 === res);
      /** Uso de expect */
      expect(res).toBeAn('number').toBe(30)
      done();
    });

    it('should square a number', done => {
      const res = utils.square(3);
      expect(res).toBe(9).toBeA('number');
      done();
    });

    // it('should expect some values', () => {
    //   /** Este tipo de aserciones funcionan bien para datos primitivos, pero no para
    //    * datos de referencia ya que, como sabemos, lo que guardan estas variables son referencias
    //    * a la ubicación en memoria de estos elementos, por lo tanto siempre una aserción
    //    * de que si dos objetos son iguales, fallará.
    //    * Para esto se utiliza el toEqual, para objetos y arreglos.
    //    */
    //   expect(12).toNotBe(11);
    //   expect({
    //     name: 'Andrew'
    //   }).toEqual({
    //     name: 'Andrew'
    //   });
    //   expect([2, 3, 5]).toInclude(5);
    //   expect([2, 3, 5]).toExclude(1);
    //   expect({
    //     name: 'Darío',
    //     age: 28
    //     /** To include revisa que exista la propiedad y la propiedad sea exactamente igual, el includeKey
    //      * Solo verifica que la propiedad exista
    //      * To excludeKeys al parecer funciona con ors, si encuentra alguna se hace verdadero aunque pienso que
    //      * no debería trabajar así. Posiblemente lo estoy ocupando mal
    //      */
    //   }).toInclude({
    //     age: 28
    //   }).toIncludeKey('age').toExclude({
    //     age: 35
    //   }).toExcludeKeys(['name', 'age', 'year']);
    // });

    it('should should verify first and last names are set', done => {
      const user = utils.setName({
        age: 27,
        location: 'México'
      }, 'Darío Navarrete');
      /** Assert it includes firstName and lastName with proper values */
      expect(user).toBeAn('object').toInclude({
        firstName: 'Darío',
        lastName: 'Navarrete'
      });
      done();
    });
  });

  describe('Async', () => {
    it('should add two numbers after a delay with a callback', done => {
      utils.asyncAdd(2, 4, result => {
        expect(result).toBe(6).toBeA('number');
        /** Es necesario llamar a done, porque mocha espera que ningún test tome más de dos
         * segundos en terminar y done le indica que el proceso async ha terminado, nada en
         * node debería tardar más de dos segundos en procesarse
         */
        done();
      })
    });

    it('should subtract two numbers after a delay with a promise', done => {
      utils.asyncSubtract(5, 3).then(res => {
        expect(res).toBeA('number').toBe(2);
        done();
      });
    });

    it('should square a number after some delay', done => {
      utils.asyncSquare(9).then(number => {
        expect(number).toBeA('number').toBe(81);
        done();
      });
    });



  });


});
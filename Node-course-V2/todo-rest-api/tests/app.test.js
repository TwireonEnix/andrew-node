const { ObjectId } = require('mongoose').Types;
const expect = require('expect');
const request = require('supertest');
const app = require('../app');
const Todo = require('../models/todo.model');

/** Dummy todos para seedear la db */
const todos = [{
  _id: new ObjectId(),
  text: 'First test todo'
}, {
  _id: new ObjectId(),
  text: 'Second test todo'
}];

/** Para poder correr bien los tests se necesita vaciar la bd antes de cada prueba, por lo
 * tanto se utilizará un life cycle hook para hacerlo
 */
beforeEach(done => {
  Todo.deleteMany({})
    .then(() => Todo.insertMany(todos))
    .then(() => done());
});

describe('POST /todos', () => {

  it('should create a new todo', done => {
    const text = 'Test todo text';
    /** Para hacer tests a los endpoints de la api, importar las funciones
     * que tienen que ver en la aplicación, y encadenar los métodos que son perfectamente
     * entendibles. (send envía datos que se parsean a json), el expect no es de la librería
     * sino que es una función de supertest, que recibe primer el estado que se espera de la
     * llamada a la api, después recibe una función que recibe el body, el cuál ya usa el
     * paquete expect para revisar si el cuerpo de la respuesta contiene el text
     */
    request(app).post('/todos').send({ text })
      .expect(200).expect(res => expect(res.body.text).toBe(text))
      .end((err, res) => {
        if (err) return done(err);
        Todo.find().then(todos => {
          expect(todos.length).toBe(3);
          expect(todos[2].text).toBe(text);
          done();
        }).catch(e => done(e));
      })
  });

  it('should not create todo with invalid body data', done => {
    request(app).post('/todos').send({}).expect(400).expect(res => expect(res.body).toIncludeKey('e'))
      .end((err, res) => {
        if (err) return done(err);
        Todo.find().then(todos => {
          expect(todos.length).toBe(2);
          done();
        })
      });
  });

});

describe('GET /todos', () => {
  it('should get all todos', done => {
    request(app).get('/todos').expect(200).expect(res => expect(res.body.todos.length).toBe(2)).end(done);
  });
});

describe('GET /todos/:id', () => {
  it('should return todo document', done => {
    request(app).get(`/todos/${todos[0]._id.toHexString()}`)
      .expect(200)
      .expect(res => {
        expect(res.body).toBeAn('object').toInclude({
          _id: todos[0]._id,
          text: todos[0].text
        })
      }).end(done);
  });

  it('should return 404 if todo not found', done => {
    request(app).get(`/todos/${(new ObjectId()).toHexString()}`)
      .expect(404).end(done);
  });

  it('should return 400 when id is not valid', done => {
    request(app).get('/todos/123abc').expect(400).end(done);
  });

});

describe('DELETE /todos/:id', () => {
  /** My solution, but is better to call the query inside the end statement as a cb */
  it('should delete a document from the db and return it as a response', done => {
    request(app).delete(`/todos/${todos[1]._id.toHexString()}`)
      .expect(200)
      .expect(result => {
        expect(result.body).toBeAn('object').toInclude({
          _id: todos[1]._id,
          text: todos[1].text
        })
      }).end((err, response) => {
        if (err) return done(err);
        Todo.find({}).then(docs => {
          expect(docs).toBeAn('array')
          expect(docs.length).toBeA('number').toBe(1);
          return Todo.findById(todos[1]._id)
        }).then(doc => {
          expect(doc).toNotExist();
          done();
        }).catch(e => done(e));
      });
  });

  it('should return 404 if todo not found', done => {
    request(app).delete(`/todos/${(new ObjectId()).toHexString()}`)
      .expect(404).end(done);
  });

  it('should respond with 400 if id is invalid', done => {
    request(app).delete('/todos/abc123').expect(400).end(done);
  });

});

describe('PATCH /todos/:id', () => {
  it('should update the todo', done => {
    const text = 'Text modified for test'
    request(app).patch(`/todos/${todos[0]._id.toHexString()}`).send({
      text,
      completed: true
    }).expect(200).expect(res => {
      expect(res.body.todo).toBeAn('object');
      expect(res.body.todo.text).toBeA('string').toBe(text);
      expect(res.body.todo.completed).toBeA('boolean').toBe(true);
      expect(res.body.todo.completedAt).toBeA('string').toExist();
    }).end(done);
  });
  it('should clear completedAt when todo is not completed', done => {
    const text = 'Text modified for test 2'
    request(app).patch(`/todos/${todos[1]._id.toHexString()}`).send({
      text,
      completed: false
    }).expect(200).expect(res => {
      expect(res.body.todo).toBeAn('object');
      expect(res.body.todo.text).toBeA('string').toBe(text);
      expect(res.body.todo.completed).toBeA('boolean').toBe(false);
      expect(res.body.todo.completedAt).toNotExist();
    }).end(done);
  });
})
const User = require('../models/user.model');

module.exports = (req, res, next) => {
  const token = req.header('x-auth');
  User.findbyToken(token).then(user => {
    if (!user) return Promise.reject();
    // console.log(user);
    req.user = user;
    // console.log(req.user);
    req.token = token;
    next();
  }).catch(e => res.status(401).json());
}
const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

/** Example of a user document
 */

// {
//   email: 'dario@example.com',
//   password: 'a hash',
//   /** arrays of objects of login objects .
//    * Access: token typé
//    * token: token hash
//   */
//   tokens: [{
//     access: 'auth',
//     token: 'another hash'
//   }]
// }

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    /** here we need custom validation to  */
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    },
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  /** Syntax to specify an array as a property */
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
});

/** Add functionality to the instance methods */
UserSchema.methods.toJSON = function () {
  const object = this.toObject();
  return {
    _id: object._id,
    email: object.email
  };
}

UserSchema.methods.generateAuthToken = function () {
  const acess = 'auth';
  const token = jwt.sign({
    _id: this._id.toHexString(),
    access: 'auth'
  }, 'abc123').toString();
  this.tokens = this.tokens.concat([{
    access: 'auth',
    token
  }]);
  return this.save().then(() => token).then(token => token);
}

/** Statics define functionality on class methods */
UserSchema.statics.findbyToken = function (token) {
  let decoded;
  try {
    decoded = jwt.verify(token, 'abc123');
  } catch (e) {
    // return new Promise((resolve, reject) => reject());
    return Promise.reject();
  }
  /** searching in nested documents */
  return this.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  })
}

UserSchema.pre('save', function (next) {
  /** if the instance has been modified in the request */
  if (this.isModified('password')) {
    bcrypt.genSalt(20).then(salt => bcrypt.hash(this.password, salt)).then(hash => {
      this.password = hash;
      next();
    })
  } else {
    next();
  }
});

module.exports = mongoose.model('User', UserSchema);
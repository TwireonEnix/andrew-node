const mongoose = require('mongoose');

module.exports = mongoose.model('Todo', {
  text: {
    type: String,
    /** El valor debe existir */
    required: true,
    minlength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: String,
    default: null
  }
});
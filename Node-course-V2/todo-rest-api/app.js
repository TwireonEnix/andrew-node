require('dotenv').config();
const morgan = require('morgan');
const express = require('express');
const app = express();
const mongoose = require('./db/mongoose');
const Todo = require('./models/todo.model');
const User = require('./models/user.model');
const authenticate = require('./middleware/authenticate');

/** El middleware paresea el body a json y lo regresa al objeto req.body */
app.use(express.json());
// app.use(morgan('dev'));

app.post('/todos', (req, res) => {
  const todo = new Todo({ text: req.body.text });
  todo.save().then(document => res.status(200).json(document))
    .catch(e => res.status(400).json({ e }));
});

app.get('/todos', (req, res) => {
  Todo.find().then(todos => res.status(200).json({ todos })).catch(e => res.status(400).json({ e }));
});

app.get('/todos/:id', (req, res) => {
  const { id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).json({ msg: 'Id was invalid' })
  Todo.findById(id).then(doc => {
    if (!doc) return res.status(404).json({ msg: 'Todo not found' })
    /** Error here. I thought calling the response ended the execution, but no, 
     * that's why cant set res headers after they were sent error popped out in my
     * other apis as well
     */
    res.status(200).json(doc);
  }).catch(e => res.status(500).json(e));
});

app.delete('/todos/:id', (req, res) => {
  const { id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).json({ msg: 'Id not valid ' });
  Todo.findByIdAndDelete(id).then(doc => {
    if (!doc) return res.status(404).json({ msg: 'Todo not found' });
    res.status(200).json(doc);
  }).catch(e => res.status(500).json(e));
});


app.patch('/todos/:id', (req, res) => {
  const { id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(400).json({ msg: 'Invalid id' })
  const updateBody = {
    text: req.body.text,
    completed: req.body.completed
  };
  if (!req.body.text) delete updateBody.text;
  if (typeof updateBody.completed === 'boolean' && updateBody.completed) {
    updateBody.completedAt = new Date().toISOString();
  } else {
    updateBody.completed = false;
    updateBody.completedAt = null;
  }
  Todo.findOneAndUpdate({ _id: id }, { $set: updateBody }, { new: true }).then(todo => {
    if (!todo) return res.status(404).json({ msg: 'Todo not found' });
    res.status(200).json({ todo });
  }).catch(e => res.status(500).json(e))
});

app.post('/users', (req, res) => {
  const newUser = new User({
    email: req.body.email,
    password: req.body.password
  });
  newUser.save().then(() => {
    /** This calls the custom method embeded on the schema */
    return newUser.generateAuthToken()
  }).then(token => {
    // console.log(token)
    /** x-header is a custom header, not natively supported by http */
    res.header('x-auth', token).status(200).json(newUser);
  }).catch(e => res.status(400).json({ e }))
});

app.get('/users/me', authenticate, (req, res) => {
  console.log(req);
  res.status(200).json({
    user: req.user
  });
});

app.listen(process.env.PORT, () => console.log('Connected to db and server in 3000'));

module.exports = app;
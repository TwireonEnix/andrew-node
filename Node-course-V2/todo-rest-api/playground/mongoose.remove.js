const mongoose = require('../db/mongoose');
const Todo = require('../models/todo.model');
const User = require('../models/user.model');
const {
  ObjectId
} = mongoose.Types.ObjectId;
const id = '5c44c73ecbb03212d17032a9';


/** Remove with no args remove all documents, remove and modify are deprecated
 * use findOneAndDelete or deleteMany instead
 */
Todo.findByIdAndRemove('5c4b5b5b20ca4b05208dd725').then(result => console.log(result));
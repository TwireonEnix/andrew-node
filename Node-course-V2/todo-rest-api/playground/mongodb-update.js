require('dotenv').config({
  path: '../.env'
});
const {
  MongoClient,
  ObjectID
} = require('mongodb');

const contStr = `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/meadCourse?retryWrites=true01`;

MongoClient.connect(contStr, {
  useNewUrlParser: true
}, (err, client) => {
  if (err) return console.log('Unable to connect to db', JSON.stringify(err, null, 2));
  console.log('Connected to Atlas');
  const db = client.db('meadCourse');

  // db.collection('Todos').findOneAndUpdate({
  //   _id: new ObjectID('5c425cba48a0ef42ad3b5eed')
  // }, {
  //   $set: {
  //     completed: true
  //   }
  // }, {
  //   returnOriginal: false
  // }).then(updatedDoc => console.log(updatedDoc)).catch(err => console.log(err));

  /** Usage of more update operators */
  db.collection('Users').findOneAndUpdate({
    _id: 123
  }, {
    /** Update operators */
    $set: {
      // _id: new ObjectID(),
      name: 'Darío',
    },
    $inc: {
      /** acepta negativos para decrementar */
      age: 2
    }
  }, {
    returnOriginal: false
  }).then(updatedDoc => console.log(updatedDoc)).catch(err => console.log(err));


  client.close();

});
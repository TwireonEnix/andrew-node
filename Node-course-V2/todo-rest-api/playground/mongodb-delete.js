require('dotenv').config({
  path: '../.env'
});
const {
  MongoClient,
  ObjectID
} = require('mongodb');
const contStr = `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/meadCourse?retryWrites=true01`;
MongoClient.connect(contStr, {
  useNewUrlParser: true
}, (err, client) => {
  if (err) return console.log('Unable to connect to db', JSON.stringify(err, null, 2));
  const db = client.db('meadCourse');

  /** Remover de la bd */
  /** deleteMany */
  // regresa json con demasiadas propiedades, pero las importantes es el deletecount, y el result.n que ambos
  // concuerdan con el número de elementos borrados

  // db.collection('Todos').deleteMany({
  //   text: 'Eat lunch'
  // }).then(result => console.log(result));

  /** deleteOne 
   * Solamente borra un registro, independientemente que haya más que cumplan el criterio
   */

  // db.collection('Todos').deleteOne({
  //   text: 'Eat lunch'
  // }).then(result => console.log(result));

  /** findOneAndDelete
   * Aquí supongo que mongoose parsea el id que recibe en sus métodos haciendo la conversión a
   * un ObjectID
   */

  // db.collection('Todos').findOneAndDelete({
  //   _id: new ObjectID('5c424efc35b2f23e88def39b')
  // }).then(result => console.log(result)).catch(err => console.log(err));

  /** Challenges: delete all duplicate users */
  // db.collection('Users').deleteMany({
  //   name: 'Darío'
  // }).then(result => console.log(result)).catch(err => console.log(err));

  /** findOneAndDelete again for id */
  db.collection('Users').findOneAndDelete({
    _id: new ObjectID('5c424140b676ab38061443ee')
  }).then(result => console.log(result)).catch(err => console.log(err));

  client.close();
});
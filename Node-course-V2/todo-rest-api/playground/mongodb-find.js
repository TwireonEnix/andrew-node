require('dotenv').config({
  path: '../.env'
});
const {
  MongoClient,
  ObjectID
} = require('mongodb');

const contStr = `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/meadCourse?retryWrites=true01`;

MongoClient.connect(contStr, {
  useNewUrlParser: true
}, (err, client) => {
  if (err) return console.log('Unable to connect to db', JSON.stringify(err, null, 2));
  console.log('Connected to Atlas');
  const db = client.db('meadCourse');
  /** Read from db 
   * Find regresa un mongodb cursor, el cursor no son los documentos, sino un puntero a esos
   * documentos, el cursor tiene muchos métodos. Ej toArray regresa todos los documentos como 
   * un arreglo. toArray regresa un promise. El find recibe como argumento un query para
   * configurar la búsqueda
   */
  db.collection('Todos').find({
    completed: false
  }).toArray().then(docs => console.log(JSON.stringify(docs, null, 2))).catch(err => console.log('Unable to fetch'));

  /** Case 2: El find por id no existe nativamente en el driver. Si se busca por id pasandole el string
   * el query no regresará nada ya que el _id asignado automáticamente es un objeto tipo ObjectId, no un
   * string primitivo.
   */
  db.collection('Todos').findOne({

    // _id: '5c42451ba0bd7141c0a3d0a8' won't work
    _id: new ObjectID('5c42451ba0bd7141c0a3d0a8')
  }).then(document => console.log(document)).catch(err => console.log('Unable to find'));

  /** Count todos */
  db.collection('Todos').find().count().then(count => console.log(`Todos count: ${count}`)).catch(err => console.log('Unable to fetch'));

  /** Query users */
  db.collection('Users').find({
    name: 'Darío'
  }).toArray().then(docs => console.log(docs)).catch(err => console.log(err));

  client.close();
});
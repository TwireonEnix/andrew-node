const mongoose = require('../db/mongoose');
const Todo = require('../models/todo.model');
const User = require('../models/user.model');
const {
  ObjectId
} = mongoose.Types.ObjectId;
const id = '5c44c73ecbb03212d17032a9';
if (!ObjectId.isValid(id)) return console.log('id not valid');
// Todo.find({
//   _id: id
// }).then(todo => console.log(todo));

// Todo.findOne({
//   _id: id
// }).then(todo => console.log(todo));

Todo.findById(id).then(todo => {
  if (!todo) return console.log('Id not found');
  console.log(todo);
}).catch(e => console.log('Id was invalid'));

User.findById(id).then(user => {
  if (!user) return console.log('Unable find user');
  console.log(user)
});
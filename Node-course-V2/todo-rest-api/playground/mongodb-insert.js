require('dotenv').config({
  path: '../.env'
});
const {
  MongoClient,
  ObjectID
} = require('mongodb');

const contStr = `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/meadCourse?retryWrites=true01`;

MongoClient.connect(contStr, {
  useNewUrlParser: true
  /** en v3 recibe err and client */
}, (err, client) => {
  if (err) return console.log('Unable to connect to db', JSON.stringify(err, null, 2));
  console.log('Connected to Atlas');
  /** Para acceder a la colección se necesita hacer referencia con el cliente */
  const db = client.db('meadCourse');
  db.collection('Todos').insertOne({
    text: 'Eat lunch',
    completed: false
  }, (err, result) => {
    if (err) return console.log('Unable to insert todo', err);
    console.log(JSON.stringify(result.ops, null, 2));
  })

  /** Primera salida del script: 
    [{
      "text": "Something to do",
      "completed": false,
      "_id": "5c42404afb59703773fca967"
    }]
  */
  // Insert new Doc into Users collection (name, age, location);

  // db.collection('Users').insertOne({

  //   name: 'Darío',
  //   age: 28,
  //   location: 'México'
  // }, (err, result) => {
  //   if (err) return console.log('Unable to insert user', err);
  //   /** result.ops es un arreglo de todo lo que se está insertando */
  //   console.log(JSON.stringify(result.ops, null, 2));
  //   console.log(result.ops[0]._id.getTimestamp());
  // })

  /** Second output:
    [{
      "name": "Darío",
      "age": 28,
      "location": "México",
      "_id": "5c424140b676ab38061443ee"
    }]
   */

  /** Cerrar la conexión al cliente */
  client.close();

});
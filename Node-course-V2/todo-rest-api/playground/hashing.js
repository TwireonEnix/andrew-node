/** Algorithm to import */
const {
  SHA256
} = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const password = '123abc';

// bcrypt.genSalt(10).then(salt => bcrypt.hash(password, salt)).then(hash => console.log(hash));

const hashedPassword = '$2a$10$Pti/r3oLDvmOebMQotEcqOMkd8hbSrSCcUjdfQQlGkyv/vCyIh83a';
bcrypt.compare(password, hashedPassword).then(match => console.log(match));
// const message = 'I am user number 3'
// /** Hashing to ofuscate any messages, SHA256 returns an object */
// const hash = SHA256(message).toString();

// console.log(`Message: ${message}`);
// console.log(`Hash: ${hash}`);

// /** data from the server to the client */
// const data = {
//   id: 4
// };

// const token = {
//   data,
//   hash: SHA256(JSON.stringify(data) + 'someSecret').toString()
// }

// /** Salting is a random generated value to add on the hash to add an extra layer of
//  * security
//  */
// const resultHash = SHA256(JSON.stringify(token.data) + 'someSecret').toString();

// resultHash === token.hash ? console.log('Data was not changed') : console.log('Data was changed. Do not trust');

/** Jwt sign takes the value and a secret, hash it and returns the token */
// const data = {
//   id: 10
// }

// const token = jwt.sign(data, '123abc');
// console.log(token);

// const decoded = jwt.verify(token, '123abc');
// console.log(decoded);
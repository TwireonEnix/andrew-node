require('dotenv').config({
  path: '../.env'
});
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const contStr = `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/meadCourse?retryWrites=true01`;

mongoose.connect(contStr, {
  useNewUrlParser: true
});
// .then(() => console.log('Connected')).catch(err => console.log(err));

module.exports = mongoose;
/** Generic files to provide base functions for all the applications: Functional Programming? */
console.log('utils.js');

const name = `Mike`;

/** This could be used as a return statement */
module.exports = { add: (a, b) => a + b, name };
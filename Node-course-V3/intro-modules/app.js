/** Naming convention to name the constants that contain the modules with the same name. */
// const fs = require('fs');

/** If file exists it will overwrite the content with the actual string. */
// fs.writeFileSync('notes.txt', 'My name is Darío.');


/** Challenge: Append a message to notes.txt */
// fs.appendFileSync('notes.txt', `\nAnd I'm a full stack javascript developer`);

/** To run a file or load a module, if it isnt a node_modules or a native one, we must provide 
 * a relative path
 */

const validator = require('validator');
const { name, add } = require('./utils');
const notesFn = require('./notes');
const chalk = require('chalk');



// console.log(name, add(1, -3));
// console.log(notesFn.getNotes());
// console.log(validator.isEmail('w@ex.com'));
console.log(chalk.red.inverse.bold.italic('Error'));

/** Process.argv is an array with the entry values of the program. the first two lines refer
 * to where the program is executing [0] -> node executable, [1] -> full path to the file that is running
 * and the third one is the one we pass it through the cli
 */
console.log(process.argv)
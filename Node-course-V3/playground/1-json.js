const fs = require('fs');

const book = {
	title: 'Ego is the Enemy',
	author: 'Ryan Holiday',
};

// This JSON Data is a simple string
const bookJSON = JSON.stringify(book);
console.log(bookJSON);

// This is a native JS object.
const parseData = JSON.parse(bookJSON);
console.log(parseData);

fs.writeFileSync('1-json.json', bookJSON);
